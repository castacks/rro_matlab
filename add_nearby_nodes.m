%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ route_struct ] = add_nearby_nodes( nodes, route_struct, reward_func_handle, D, budget, threshold_distance, threshold_benefit,w )
%ADD_NEARBY_NODES Summary of this function goes here
%   Detailed explanation goes here
dimensionality = size(nodes,2);

if nargin < 8
    w=1;
end

nodes(:,end+1) = 1:size(nodes,1);
route = route_struct.route;
id = nodes(:,end);

if dimensionality>2
    d = distance_from_route(nodes(:,1:3),route, nodes(:,1:3),w);
else
    d = distance_from_route(nodes(:,1:2),route, nodes(:,1:2),w);
end

id(route) = [];
d(route) = [];
I = find(d<= threshold_distance);
if(~isempty(I))
    index = id(I(:));
    start_node_id = route_struct.route(1);
    end_node_id = route_struct.route(end);
    if(size(index,1)>1)
        index = index';
    end
    nodes(:,end) = [];
    benefits = zeros(size(index));
    for i=1:length(index)        
        [reward, ~] = reward_func_handle(nodes(index(i),:), route_struct.reward_func_input);
        benefit = reward/d(i);
        if(benefit> threshold_benefit)
            benefits(i) = reward/d(i);
        end
    end
    [benefits,I] = sort(benefits,'descend');
    index = index(I);
    d = d(I);
    d(benefits==0)=[];
    index(benefits==0) = [];    
    dd = cumsum(2*d);
    index(dd>(budget-route_struct.cost)) = [];    
    [route_struct.route, route_struct.cost] = tsp(D, start_node_id, end_node_id,[index,route_struct.route]);
    [reward, route_struct.reward_func_input] = reward_func_handle(nodes(index,:), route_struct.reward_func_input);
    route_struct.reward = reward + route_struct.reward;
end
        
end