%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
working_dir = pwd;
addpath(strcat(pwd,'/display_utils'));
addpath(strcat(pwd,'/reward_functions'));
addpath(strcat(pwd,'/sampling_functions'));
addpath(strcat(pwd,'/route_selection_functions'));
addpath(strcat(pwd,'/tsp'));
addpath(strcat(pwd,'/utils'));