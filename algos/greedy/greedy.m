function [ route_list, best_route_id ] = greedy(  nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, display_flag, display_func,mode,w )
%GREEDY_ALGO Summary of this function goes here
%   Detailed explanation goes here
if nargin <11
    w=1;
end
[route_list.route, route_list.cost] = tsp(D, start_node_id, end_node_id,[]);
route_list.reward_func_input = [];
best_route_id = [];
if route_list.cost <= budget
   [route_list.reward, route_list.reward_func_input] = reward_func_handle(nodes(route_list.route,:), route_list.reward_func_input);
   sampled_route = route_list;
   best_route_id =  1;
else
    return;
end

if(display_flag)
    active_fig_handle = figure;
end

run_time = 0;
best_route_id = 1;
counter=1;
max_route_reward = route_list.reward;
while (run_time < run_time_budget)
    if(display_flag)
        display_func(counter, nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, 1, active_fig_handle )
        pause(0.1)
    end
    tic
    rewards = zeros(size(nodes,1),1);
    distances = rewards;
    node_id_list = 1:size(nodes,1);
    for i=1:size(nodes,1)
        if(isempty(route_list.route(i==route_list.route)))
            if(mode==0)
                [rewards(i),~] = reward_func_handle(nodes(i,:), route_list.reward_func_input);
            else
                [rewards(i),~] = reward_func_handle(nodes(i,:), []);
            end
            distances(i) = distance_from_route( nodes(i,:), route_list.route, nodes,w );
            if(distances(i)==0)
                distances(i) = 10^(-6);
            end
        end
    end
    
    reward_per_distance = rewards./distances;
    node_id_list(isnan(reward_per_distance)) = [];
    reward_per_distance(isnan(reward_per_distance))=[];
    
    [reward_per_distance,ID] = sort(reward_per_distance,'descend');
    node_id_list = node_id_list(ID);
    
    for i=1:length(reward_per_distance)
        temp_route = route_list.route;
        temp_route(end) = node_id_list(i);
        temp_route(end+1)= end_node_id;
        route = temp_route;
        cost = 0;
        for j=1:(length(route)-1)
            cost = cost + D(route(i),route(i+1));
        end
        if cost<=budget
            route_list.route = route;
            route_list.cost = cost;
            [route_list.reward,route_list.reward_func_input] = reward_func_handle(nodes(route_list.route,:), []);
            break;
        end
    end
    
    sprintf('Reward:%f Cost:%f Size of route:%f', route_list(best_route_id).reward, route_list(best_route_id).cost, length(route_list(best_route_id).route))
    sprintf('Runtime:%f',run_time)
    run_time = run_time + toc;
    counter = counter+1;

end


end

