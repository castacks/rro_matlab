function [ route_list, best_route_id ] = make_route_list(V_c,V,budget)
%MAKE_ROUTE_LIST Summary of this function goes here
%   Detailed explanation goes here
route_list= []; 
best_route_id = 0;
max_reward = 0;
for i=1:size(V_c,1)
    reward = V_c(i,3);
    cost = V_c(i,2);
    route = V_c(i,1);
    j=V_c(i,4);
    not_done = 1;
    while(not_done>0)
        route = [V(j,1),route];
        j=V(j,4);
        not_done = j;
    end
    temp_route_list.reward = reward;
    temp_route_list.cost = cost;
    temp_route_list.route = route;
    if(temp_route_list.cost>budget)
        continue;
    end
    if(isempty(route_list))
        route_list = temp_route_list;
    else
        route_list(end+1) = temp_route_list;
    end
    if(route_list(end).reward > max_reward)
        best_route_id = length(route_list);
        max_reward = route_list(end).reward;
    end
end
end