function [ to_prune ] = prune( n_new, n_new_reward, V, nodes, D, budget,reward_func_handle )
%PRUNE Summary of this function goes here
%   Detailed explanation goes here
node_ids = 1:size(nodes,1);
to_prune=0;
V = V(V(:,1)==n_new(1),:);
if(isempty(V))
    return;
end
V = V(V(:,2)<= n_new(2),:);
if(isempty(V))
    return;
end

V = V(V(:,3)>= n_new(3),:);
if(isempty(V))
    return;
end

 d = D(n_new(1),:);
 node_ids = node_ids(d<(0.5*(budget-n_new(2))));
 
 [reward,~] = reward_func_handle(nodes(node_ids,:),n_new_reward);
 
 upper_bound = reward + n_new(3);
 
 if(isempty(V(V(:,3)>(upper_bound+n_new(3)),:)))
     return;
 end
 
 to_prune = 1;

end

