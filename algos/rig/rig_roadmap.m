%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [route_list, best_route_id] = rig_roadmap( nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, near_radius, end_pt_sampling_rate, display_flag, display_func)
%RRO Summary of this function goes here
%   Detailed explanation goes here
    %in this algorithm route_list is just the routes explored, they may not
    %reach the end so be careful. Will do this last
        
   [reward, reward_func_input] = reward_func_handle(nodes(start_node_id,:), []);
   V = [start_node_id, 0, reward, 0]; % node_id,cost,reward,parent_id
   V_reward(1) = reward_func_input;
   
   V_c = [];
   
   best_route_id = [];

	if(display_flag)
        active_fig_handle = figure;
	end

   run_time = 0;


    best_route_id = [];
    max_route_reward = 0;
    route_list = [];
    
    sampling_func_input.reward_func_handle = reward_func_handle;
    sampling_func_input.sampling_func_ip = [];
    sampling_func_input.useless_node_ids = [find(D(start_node_id,:)>budget*0.5)';...
                                        find(D(end_node_id,:)>budget*0.5)'];


    counter = 1;
    node_id = [];
    best_route_id  = [];
    route_list = [];
    max_route_reward = 0;
    while (run_time < run_time_budget)
        if(display_flag && ~isempty(route_list))
			display_func(counter, nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, active_fig_handle )
			pause(0.1)
        end
        tic
        if(rem(counter,end_pt_sampling_rate)==1)
            node_id = end_node_id;
        else
            [node_id, sampling_func_input] = sampling_func_handle(nodes,sampling_func_input);
        end
        
        % find near, nearest is always in near.
        vids = 1:size(V,1);
        vids(V(:,1)==node_id)= [];
        near_nodes_d = D(V(vids(:),1),node_id);
        [~,nearest_ID] = min(near_nodes_d);
        nearest_vid = vids(nearest_ID);
        vids = vids(near_nodes_d<= near_radius);
        near_node_vids = unique([nearest_vid,vids]);
        
        % going through all near nodes
        for i=1:length(near_node_vids)
            [reward, reward_func_input] = reward_func_handle(nodes(node_id,:), V_reward(near_node_vids(i)));
            path_length = V(near_node_vids(i),2) + D(V(near_node_vids(i),1),node_id);
            %compiling all info
            n_new = [node_id, path_length, reward+V(near_node_vids(i),3), near_node_vids(i)];
            n_new_reward = reward_func_input;
            % Pruning
            if(prune(n_new, n_new_reward, V, nodes, D, budget,reward_func_handle)==0)
                if(path_length>=budget || node_id == end_node_id)
                    V_c = [V_c;n_new];
                else
                    V = [V;n_new];
                    V_reward = [V_reward; n_new_reward];
                end
            end            
        end        
        [route_list, best_route_id ] = make_route_list(V_c,V,budget);    
        if(~isempty(route_list))% && route_list(best_route_id).reward>max_route_reward)
            %route_list(route_id).run_time = run_time;            
            max_route_reward = route_list(best_route_id).reward;
            sprintf('Reward:%f Cost:%f Size of route:%f', route_list(best_route_id).reward, route_list(best_route_id).cost, length(route_list(best_route_id).route))
            sprintf('Runtime:%f',run_time)
        end
        run_time = run_time + toc;
		counter = counter+1;
    end        
end