%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
clear
close all

add_to_path

%% setting up reward function
func_state = [];
num_nodes_per_cell = 1;


%% setting up sampling function
sampling_func_handle = @(nodes, sampling_func_input)(weighted_node_sampling(nodes,sampling_func_input));

%% setting up route selection function handle
%route_selection_func_handle = @distance_based_route_selector;
route_selection_func_handle = @best_rate_path_selection;

%% RRO
% In this scenario there is a of nodes that are of high value and are far
% away from the start. If not selected will lead to a highly sub-optimal path.
budget = 70;
bin_size = 1;
[X,Y] = meshgrid(0.5:2*bin_size:10, 0.5:2*bin_size:10);
nodes = [X(:), Y(:)];

nodes = [nodes; [14.5,0.5]; [0.5,14.5]; [14.5, 14.5]; [0.51,0.51]];
nodes = [nodes, ones(size(nodes,1),1)];

weights = 0.1*ones(10,10);
weights(15,1) = 10; weights(1,15) = 10; weights(15,15) = 10;

D = distmat(nodes);
start_node_id = 1;
end_node_id = 2;
run_time_budget = 30;
reward_func_handle = @(nodes, func_state) (weighted_binning_submodular( nodes, func_state, bin_size, weights, num_nodes_per_cell));
threshold_distance = 0.5;
display_flag = 1;
display_func = @(counter,nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, active_fig_handle) (display_grid(counter, nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, active_fig_handle));
[route_list, best_route_id] = rro( nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, route_selection_func_handle, threshold_distance, display_flag, display_func);