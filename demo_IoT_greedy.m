%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
clear all
close all

add_to_path
addpath(strcat(pwd,'/reward_functions/IoT'));
addpath(strcat(pwd,'/reward_functions/IoT/display'));
addpath(strcat(pwd,'/algos/greedy'));
%% setting up reward function
func_state = [];
sensor_locations = [229,305; 228,305;316,78; 330,15; 296,22; 216,23; 216,4 ;276,127; 249,65; 267,65;275,110;296,4;296,37];
sensor_locations = [sensor_locations(:,2),sensor_locations(:,1)];

dense_sensor_locations =[42,366; 62,366; 84,368; 62,350;...
                        83,353; 104,352; 124,352; 145,351;...
                        82,337; 105,337; 125,338; 145,337;...
                        163,338; 104,320; 126,323; 145,323;...
                        162,322; 162,307; 145,307; 126,307;...
                        126,297; 145,292; 164,293; 164,279;...
                        147,281; 128,279; 25,366; 174,382;...
                        125,382];
dense_sensor_locations(:,2) = dense_sensor_locations(:,2) + 134;                    
sensor_locations = [sensor_locations; dense_sensor_locations(:,2),dense_sensor_locations(:,1)];

im = imread('scenario2.png');
im = rgb2gray(im);
%im = bwmorph(im,'thicken');
im(im<255) = 0;
threshold = 10;

sensor_size = [-20,20,-20,20];
cached_sensor_coverage = initialize_coverage_map( sensor_locations, sensor_size, im,threshold);
weights = ones(size(im));

reward_func_handle = @(nodes, func_state) (sensor_coverage( nodes, func_state, sensor_locations, weights, cached_sensor_coverage));

%% setting up display
display_func = @(counter,nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, active_fig_handle) (display_IoT_rro( counter,nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, active_fig_handle, im, cached_sensor_coverage));

%% Greedy
budget =  1250;
nodes = [sensor_locations(:,1), sensor_locations(:,2)];
D = distmat(nodes);
start_node_id = 1;
end_node_id = 2;
run_time_budget = 10;
threshold_distance = 0.015*budget;
threshold_benefit = 10;
display_flag = 1;
mode = 1;
tic
toc
[route_list, best_route_id] = greedy(nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, display_flag, display_func, mode);