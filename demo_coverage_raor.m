%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
clear all
close all

add_to_path
addpath(strcat(pwd,'/reward_functions/map_coverage'));
addpath(strcat(pwd,'/reward_functions/map_coverage/display'));

%% setting up map
image_path = 'semantic_map.png';
sub_sample = 1;
[ map ] = get_multiclass_map_from_image( image_path, sub_sample);
map_struct.scale = 1; map_struct.min = [1,1];

%% setting up observation
camera_fov = deg2rad(25);
camera_handle = @(pose,map_struct_var)(get_circular_camerafootprint( pose, camera_fov, map_struct));
allowed_distances = [20;100];


%% setting up reward function
low_sensors = 10; high_sensors = 55;
[X,Y] = meshgrid(1:4:size(map,1),1:4:size(map,2));%low sensors
sensor_locations = [X(:),Y(:),low_sensors*ones(size(X(:),1),1)];
[X,Y] = meshgrid(1:20:size(map,1),1:20:size(map,2));% high sensors
temp_sensor_locations = [X(:),Y(:),high_sensors*ones(size(X(:),1),1)];
sensor_locations = [sensor_locations;temp_sensor_locations];
%% caching visibility data
[ cached_visibility ] = cache_visibility( sensor_locations, map, map_struct, allowed_distances, camera_handle );
%% checking reward function
weights = map;
high_reward = 10;
low_reward = 1;
weights(map==0) = high_reward;
weights(map==1) = low_reward;
func_state =[];
reward_func_handle = @(nodes, func_state) (map_coverage_reward( nodes, func_state, sensor_locations, weights, cached_visibility));

w =3;
%% setting up sampling function
sampling_func_handle = @(nodes, sampling_func_input)(weighted_node_sampling(nodes,sampling_func_input));

%% setting up route selection function handle
%route_selection_func_handle = @distance_based_route_selector;
route_selection_func_handle = @best_rate_path_selection;

%% setting up display
display_func = @(counter,nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, active_fig_handle) (display_coverage_rro( counter,nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, active_fig_handle, map, cached_visibility));

%% RAOr
budget = 510;
nodes = [sensor_locations(:,1), sensor_locations(:,2), sensor_locations(:,3)];
D = distmat(nodes,w);
start_node_id = 1;
end_node_id = 2;
run_time_budget = 300;
threshold_distance = 2;
threshold_benefit = 1;
display_flag = 1;
tic
toc
[route_list, best_route_id] = rro( nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, route_selection_func_handle, threshold_distance, threshold_benefit, display_flag,display_func,w);