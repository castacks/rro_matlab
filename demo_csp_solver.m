%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
clear
close all

%add_to_path

%% setting up reward function
% func_state = [];
% bin_size = 1;
% max_xy = [10, 10];
% num_nodes_per_cell = 1;
% 
% reward_func_handle = @(nodes, func_state) (binary_binning_submodular( nodes, func_state, bin_size, max_xy, num_nodes_per_cell));
% %weighted_binning_submodular( node, func_state, bin_size, weights, num_nodes_per_cell)
% 
% %% setting up sampling function
% sampling_func_handle = @(nodes, sampling_func_input)(weighted_node_sampling(nodes,sampling_func_input));
% 
% %% setting up route selection function handle
% budget = 50;
% %route_selection_func_handle = @distance_based_route_selector;
% route_selection_func_handle = @best_rate_path_selection;
% 
% %% RRO
% [X,Y] = meshgrid(0.5:bin_size:max_xy(1), 0.5:bin_size:max_xy(2));
% nodes = [X(:), Y(:)];
% D = distmat(nodes);
% start_node_id = 1;
% end_node_id = 2;
% run_time_budget = 15;
% display_flag = 1;
% threshold_distance = 0.5;
% [route_list, best_route_id] = rro( nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, route_selection_func_handle, threshold_distance, display_flag);

%% Alright lets try the weighted cost function
clear
close all

%add_to_path

%% setting up reward function
func_state = [];
bin_size = 1;
max_xy = [10, 10];
num_nodes_per_cell = 1;

reward_func_handle = @(nodes, func_state) (binary_binning_submodular( nodes, func_state, bin_size, max_xy, num_nodes_per_cell));
%weighted_binning_submodular( node, func_state, bin_size, weights, num_nodes_per_cell)

%% setting up sampling function
sampling_func_handle = @(nodes, sampling_func_input)(weighted_node_sampling(nodes,sampling_func_input));

%% setting up route selection function handle
budget = 15;
%route_selection_func_handle = @distance_based_route_selector;
route_selection_func_handle = @best_rate_path_selection;

%% RRO
% [X,Y] = meshgrid(0.5:bin_size:max_xy(1), 0.5:bin_size:max_xy(2));
% nodes = [X(:), Y(:)];
% D = distmat(nodes);
% start_node_id = 1;
% end_node_id = 2;
% run_time_budget = 10;
% display_flag = 0;
% threshold_distance = 0.5;
% %[route_list, best_route_id] = rro( nodes, D, s  tart_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, route_selection_func_handle, threshold_distance, display_flag);
% [route_list, best_route_id] = randomized_csp_solver( nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, display_flag);

% %% Alright lets try the weighted cost function
% % In this scenario there is a of nodes that are of high value and are far
% % away from the start. If not selected will lead to a highly sub-optimal path.
close all
weights = 0.1*ones(15,15);
weights(15,1) = 10; weights(1,15) = 10; weights(15,15) = 10;
budget = 70;
bin_size = 1;
[X,Y] = meshgrid(0.5:2*bin_size:max_xy(1), 0.5:2*bin_size:max_xy(2));
nodes = [X(:), Y(:)];

nodes = [nodes; [14.5,0.5]; [0.5,14.5]; [14.5, 14.5]; [0.51,0.51]];
nodes = [nodes, ones(size(nodes,1),1)];

D = distmat(nodes);
start_node_id = 1;
end_node_id = 2;
run_time_budget = 5;
reward_func_handle = @(nodes, func_state) (weighted_binning_submodular( nodes, func_state, bin_size, weights, num_nodes_per_cell));
threshold_distance = 0.5;
display_flag = 0;
[route_list, best_route_id] = randomized_csp_solver( nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, display_flag);