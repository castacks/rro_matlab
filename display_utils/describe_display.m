%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function describe_display( init_pos, start_node, end_node )
%DISPLAY_GRAPH Summary of this function goes here
%   Detailed explanation goes here
    plot(init_pos(1), init_pos(2),'.','MarkerSize',60,'MarkerEdgeColor',[0.2 0.7 0.2]) % start/end position
    
    best_route = [init_pos(1), init_pos(2)-1; init_pos(1)+2, init_pos(2)-1];
    plot(best_route(:,1), best_route(:,2),'LineWidth', 4,'Color', [0.4 0.5 0.8]) % best route
    plot(best_route(:,1), best_route(:,2),'.','MarkerSize',60,'MarkerEdgeColor',[0.6 0.6 0.6]) % best route
    plot(best_route(:,1), best_route(:,2),'.','MarkerSize',30,'MarkerEdgeColor',[0.2 0.2 0.2]) % best route
    
    plot(init_pos(1), init_pos(2)-2,'.','MarkerSize',60,'MarkerEdgeColor',[0.8 0.2 0.2]) % sampled node
    
    searched_route = [init_pos(1), init_pos(2)-3; init_pos(1)+2, init_pos(2)-3];
    plot(searched_route(:,1), searched_route(:,2),'LineWidth',4, 'Color', [0.9 0.9 0.9])
    plot(searched_route(:,1), searched_route(:,2),'.','MarkerSize',30,'MarkerEdgeColor',[0.6 0.6 0.6]) % searched routes
   
    plot(init_pos(1), init_pos(2)-4,'*','LineWidth',4,'MarkerSize',25,'MarkerEdgeColor',[0.2 0.2 0.2])
    
    high_value_nodes  = [[14.5,0.5]; [0.5,14.5]; [14.5, 14.5]];
    plot(high_value_nodes(:,1), high_value_nodes(:,2),'*','LineWidth',4,'MarkerSize',25,'MarkerEdgeColor',[0.2 0.2 0.2])
    
    plot(init_pos(1), init_pos(2)-4,'.','MarkerSize',30,'MarkerEdgeColor',[0.6 0.6 0.6])
    plot(init_pos(1), init_pos(2)-5,'.','MarkerSize',30,'MarkerEdgeColor',[0.6 0.6 0.6])
    plot(start_node(1), start_node(2),'.','MarkerSize',60,'MarkerEdgeColor',[0.2 0.7 0.2])
    plot(start_node(1), start_node(2),'.','MarkerSize',30,'MarkerEdgeColor',[0.1 0.5 0.1])
    plot(end_node(1), end_node(2),'.','MarkerSize',60,'MarkerEdgeColor',[0.2 0.7 0.2])
    plot(end_node(1), end_node(2),'.','MarkerSize',30,'MarkerEdgeColor',[0.1 0.5 0.1])

    
    text(init_pos(1)+2.5,init_pos(2),'Start/End Position','FontSize', 14)
    text(init_pos(1)+2.5,init_pos(2)-1,'Best Route','FontSize', 14)
    text(init_pos(1)+2.5,init_pos(2)-2,'Sampled Route','FontSize', 14)
    text(init_pos(1)+2.5,init_pos(2)-3,'Searched Routes','FontSize', 14)
    text(init_pos(1)+2.5,init_pos(2)-4,'Node Reward: 10','FontSize', 14)
    text(init_pos(1)+2.5,init_pos(2)-5,'Node Reward: 0.1','FontSize', 14)
    %text(init_pos(1)+2.5,init_pos(2)-4,'Node Reward: 1.0','FontSize', 14)
    %text(2.0,14,'Find a route that maximizes reward within','FontSize', 24)
    %text(2.0,12,'a pathlength budget of 50','FontSize', 24)
end
