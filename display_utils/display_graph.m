%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function display_graph( nodes,route_list, best_route_id, sampled_node_id )
%DISPLAY_GRAPH Summary of this function goes here
%   Detailed explanation goes here
if ~isempty(best_route_id)
    best_id = route_list(best_route_id).route;
    start_end = [best_id(1), best_id(end)];
    
    plot3(nodes(sampled_node_id,1), nodes(sampled_node_id,2),nodes(sampled_node_id,3),'.','MarkerSize',60,'MarkerEdgeColor',[0.8 0.2 0.2])
else
    best_id = [];
    start_end = [];
    sampled_node_id = []; 
end
grid on
hold on
if ~isempty(best_route_id)
    for j=1:length(route_list)
        route = route_list(j).route;
%        plot3(nodes(route,1), nodes(route,2),nodes(route,3),'LineWidth',4, 'Color', [0.9 0.9 0.9])
    end

    route  = route_list(best_route_id).route;
else
    route = [];
end
plot3(nodes(route,1), nodes(route,2),nodes(route,3),'LineWidth',4, 'Color', [0.4 0.5 0.8])
plot3(nodes(best_id,1), nodes(best_id,2),nodes(best_id,3),'.','MarkerSize',60,'MarkerEdgeColor',[0.2 0.2 0.2])
plot3(nodes(start_end,1), nodes(start_end,2),nodes(start_end,3),'.','MarkerSize',60,'MarkerEdgeColor',[0.2 0.7 0.2])

plot3(nodes(:,1), nodes(:,2), nodes(:,3),'.','MarkerSize',10,'MarkerEdgeColor',[0.6 0.6 0.6]);
plot3(nodes(sampled_node_id,1), nodes(sampled_node_id,2),nodes(sampled_node_id,3),'.','MarkerSize',60,'MarkerEdgeColor',[0.8 0.2 0.2])
plot3(nodes(start_end,1), nodes(start_end,2),nodes(start_end,3),'.','MarkerSize',30,'MarkerEdgeColor',[0.1 0.5 0.1])
axis equal
end
