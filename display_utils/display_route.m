%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function display_route( nodes, route, c )
%DISPLAY_ROUTE Summary of this function goes here
%   Detailed explanation goes here
    for i=1:max(size(route))
        id = route(i);
        id1 = rem(i+1,max(size(route))+1);
        id1 = max(id1,1);
        id1 = route(id1);
        plot(nodes(id1,1), nodes(id1,2),'LineWidth',4, 'Color', c)
    end       
    
end
% [0.4    0.5    0.8] [0.8 0.8 0.8]