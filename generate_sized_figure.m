function generate_sized_figure(width,height,type)
set(gcf, 'PaperUnits', 'inches')

papersize = get(gcf, 'PaperSize');

left = (papersize(1)- width)/2;

bottom = (papersize(2)- height)/2;


myfiguresize = [left, bottom, width, height];
set(gcf, 'PaperPosition', myfiguresize);
if (strcmp(type,'image'))
    print(gcf,'image.tiff','-dtiff')
else 
    print(gcf,'image.pdf','-dpdf')
end
