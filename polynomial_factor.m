clear all
c = @(n,a)(factorial(n)/(factorial(round(a*n))*factorial(round(n-a*n))));
h = @(x)(-x.*log(x)-(1-x).*log(1-x));
p_factor = @(a,n)(2.^(h(a).*n));

n = 1000;
alpha = 0.1;
orig = c(n,alpha);
approx = p_factor(alpha,n);

for n=10:10:100
    for a=0.1:0.1:0.9        
        orig = c(n,a);
        approx = p_factor(a,n);
        error = orig - approx;
        if error < 0
            display('Phas Gaya')
        end
    end
end