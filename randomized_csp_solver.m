%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [route_list, best_route_id] = randomized_csp_solver( nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, display_flag)
%RRO Summary of this function goes here
%   Detailed explanation goes here
    best_route_id = [];

	if(display_flag)
        active_fig_handle = figure;
	end

    route_list.reward_func_input = [];
    [route_list.route, route_list.cost] = tsp(D, start_node_id, end_node_id,[]);
    [route_list.reward, route_list.reward_func_input] = reward_func_handle(nodes(route_list.route,:), route_list.reward_func_input);
    run_time = 0;

    if(route_list.cost >= budget)
        return;
    end

    best_route_id = 1;
    max_route_reward = route_list.reward;
    
    const_func_handle = @(nodes, func_state) (constant_reward_func( nodes, func_state, [],[], []));
    sampling_func_input.reward_func_handle = const_func_handle;
    sampling_func_input.sampling_func_ip = [];
    sampling_func_input.useless_node_ids = [];

    counter = 1;
    node_id = [];
    best_route_id  = [];
    while (run_time < run_time_budget)
        if(display_flag)
			set(0,'CurrentFigure',active_fig_handle);
    		clf
			hold on
            describe_display( [17, 4], nodes(start_node_id,:), nodes(end_node_id,:));
            display_graph( nodes,route_list, best_route_id, node_id );
            text(17,7,sprintf('Reward:%2.1f ', route_list(best_route_id).reward), 'FontSize', 14)
            text(17,9,sprintf('Cost:%2.1f ', route_list(best_route_id).cost), 'FontSize', 14)
            text(17,8,sprintf('Run Time:%2.1f ', run_time), 'FontSize', 14)
            axis equal
            axis([0 23 -1 16])
            set(gcf, 'Position', [100, 100, 1080, 700]);
            set(gcf,'PaperPositionMode','auto')
            print(strcat('results/',num2str(counter,'%04d')),'-dpng','-r0')
			pause(0.1)
        end
        tic
        [node_id, sampling_func_input] = sampling_func_handle(nodes,sampling_func_input);
        route_list = flip_belongingness( node_id, route_list, D ,start_node_id, end_node_id, nodes, reward_func_handle);       
        
        if(route_list(end).reward > max_route_reward && route_list(end).cost < budget)
            best_route_id = length(route_list);
            max_route_reward = route_list(end).reward;
            sprintf('Reward:%f Cost:%f Size of route:%f', route_list(best_route_id).reward, route_list(best_route_id).cost, length(route_list(best_route_id).route))
            sprintf('Run Time:%f', run_time)
        end
        

        run_time = run_time + toc;
		counter = counter+1;
        
    end

end