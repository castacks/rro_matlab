load graph_stuff

plot(budget,value_v_budget(:,1),'LineWidth',2)
hold on
plot(budget,value_v_budget(:,2),'LineWidth',2)
plot(budget,value_v_budget(:,3),'LineWidth',2)
plot(budget,value_v_budget(:,4),'LineWidth',2)
xlabel('Budget(m)')
ylabel('Reward')
legend('Greedy','GCB','RAOr','RIG')
figure
plot(budget,time_v_budget(:,1),'LineWidth',2)
hold on
plot(budget,time_v_budget(:,2),'LineWidth',2)
plot(budget,time_v_budget(:,3),'LineWidth',2)
plot(budget,time_v_budget(:,4),'LineWidth',2)
xlabel('Budget(m)')
ylabel('Run time (s)')
legend('Greedy','GCB','RAOr','RIG')