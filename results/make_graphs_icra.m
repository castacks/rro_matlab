%load result_icra
%% reward
plot(gcb_w1(:,1),gcb_w1(:,2),'--r','LineWidth',4)
hold on
plot(rig_w1(:,1),rig_w1(:,2),'--g','LineWidth',2)
plot(greedy_w1(:,1),greedy_w1(:,2),'--b','LineWidth',2)
plot(raor_w1(:,1),raor_w1(:,2),'--c','LineWidth',2)
xlabel('Budget(m)')
ylabel('Reward')
legend('GCB','RIG','Greedy','RAOr')
grid on
hold on
plot(gcb_w3(:,1),gcb_w3(:,2),'r','LineWidth',4)
hold on
plot(rig_w3(:,1),rig_w3(:,2),'g','LineWidth',4)
plot(greedy_w3(:,1),greedy_w3(:,2),'b','LineWidth',4)
plot(raor_w3(:,1),raor_w3(:,2),'c','LineWidth',4)
%% run time
 figure
plot(gcb_w1(:,1),gcb_w1(:,3),'--r','LineWidth',2)
hold on
plot(rig_w1(:,1),rig_w1(:,3),'--g','LineWidth',2)
plot(greedy_w1(:,1),greedy_w1(:,3),'--b','LineWidth',2)
plot(raor_w1(:,1),raor_w1(:,3),'--c','LineWidth',2)
xlabel('Budget(m)')
ylabel('RunTime(s)')
legend('GCB','RIG','Greedy','RAOr')
grid on
hold on
plot(gcb_w3(:,1),gcb_w3(:,3),'r','LineWidth',2)
hold on
plot(rig_w3(:,1),rig_w3(:,3),'g','LineWidth',2)
plot(greedy_w3(:,1),greedy_w3(:,3),'b','LineWidth',2)
plot(raor_w3(:,1),raor_w3(:,3),'c','LineWidth',2)
