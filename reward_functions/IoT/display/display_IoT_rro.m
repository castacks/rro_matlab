function display_IoT_rro( counter,nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, fig_handle, im, cached_sensor_coverage)
%DISPLAY_IOT_RRO Summary of this function goes here
%   Detailed explanation goes here
set(0,'CurrentFigure',fig_handle);
clf
hold on
%describe_display( [17, 4], nodes(start_node_id,:), nodes(end_node_id,:));
best_id = [];
if ~isempty(best_route_id)
    best_id = route_list(best_route_id).route;
end

display_coverage_map( im, best_id, nodes,cached_sensor_coverage);
display_graph( nodes,route_list, best_route_id, node_id );
%text(17,7,sprintf('Reward:%2.1f ', route_list(best_route_id).reward), 'FontSize', 14)
%text(17,9,sprintf('Cost:%2.1f ', route_list(best_route_id).cost), 'FontSize', 14)
%text(17,8,sprintf('Run Time:%2.1f ', run_time), 'FontSize', 14)

axis equal
grid on
%axis([0 27 -2 16])
set(gcf, 'Position', [100, 100, 1080, 700]);
set(gcf,'PaperPositionMode','auto')
print(strcat('results/',num2str(counter,'%04d')),'-dpng','-r0')
end