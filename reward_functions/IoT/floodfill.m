function [ IND] = floodfill( bw_image, location, bbox, threshold )
%FLOODFILL Summary of this function goes here
%   Detailed explanation goes here
open_list = location;
processed_list = [];
x_limits = [location(1)+bbox(1), location(1)+bbox(2)];
y_limits = [location(2)+bbox(3), location(2)+bbox(4)];
IND = [];
while ~isempty(open_list)
    ele = open_list(1,:);
    if bw_image(ele(1),ele(2))>threshold
        IND = [IND;sub2ind(size(bw_image),ele(1),ele(2))];            
        processed_list = [processed_list;ele];
        to_add = ele - [1,0];
        open_list = add2open(to_add,open_list, processed_list ,x_limits,y_limits);
        to_add = ele - [0,1];
        open_list = add2open(to_add,open_list, processed_list ,x_limits,y_limits);
        to_add = ele + [1,0];
        open_list = add2open(to_add,open_list, processed_list ,x_limits,y_limits);
        to_add = ele + [0,1];
        open_list = add2open(to_add,open_list, processed_list ,x_limits,y_limits);         
    end
    open_list(1,:) = [];
    %size(open_list)
    %size(processed_list)
end

end

function [open_list] = add2open(to_add,open_list, processed_list ,x_limits,y_limits)
if (sum(ismember(open_list,to_add,'rows'))>0) || (sum(ismember(processed_list,to_add,'rows'))>0)
    return;
end
if to_add(1)<x_limits(1) || to_add(1)>x_limits(2) || to_add(2)<y_limits(1) || to_add(2)>y_limits(2)
    return;
end
open_list = [open_list;to_add];
end