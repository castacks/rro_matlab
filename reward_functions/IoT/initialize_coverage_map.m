function [ cached_sensor_coverage ] = initialize_coverage_map( sensor_locations, sensor_size, bw_image, threshold)
%INITIALIZE_COVERAGE_MAP Summary of this function goes here
%   Detailed explanation goes here
for i=1:size(sensor_locations,1)
    IND = floodfill( bw_image, sensor_locations(i,:), sensor_size, threshold );
    cached_sensor_coverage{i} = IND;
end

end

