%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [value, func_state] = sensor_coverage( node, func_state, sensor_locations, weights, cached_sensor_coverage)
%BINARY_BINNING Summary of this function goes here
%   Detailed explanation goes here
% we will get n^2
if (isempty(cached_sensor_coverage) || (size(cached_sensor_coverage,2) ~= size(sensor_locations,1)))
    error('Sensor Coverage function needs cached_sensor_coverage with correct dimnesionality.')
else

if(isempty(func_state) || isempty(func_state.visited_map))
    func_state.visited_map = zeros(size(weights));
end
node = [node(:,1), node(:,2)];

node(node(:,1)>size(weights,1),:) = [];
node(node(:,2)>size(weights,2),:) = [];

node(node(:,1)<0,:) = [];
node(node(:,2)<0,:) = [];

n = ceil(node(:,1:2));

n_ind = sub2ind(size(weights),node(:,1),node(:,2));
s_ind = sub2ind(size(weights), sensor_locations(:,1), sensor_locations(:,2));
ind_list = [];
for i=1:size(sensor_locations,1)
   count = sum(n_ind==s_ind(i));
   if count > 0
       ind_list = [ind_list;cached_sensor_coverage{i}];
   end
end
ind_list = unique(ind_list);
ind_list(func_state.visited_map(ind_list)>0) = [];

new_visits = weights(ind_list);
value = sum(new_visits);

func_state.visited_map(ind_list) = 1;
end