clear
close all
clc
sensor_locations = [113,129; 98,99; 172,138; 155,110; 142,82; 215,119; 200,92; 184,66 ];
sensor_locations = [sensor_locations(:,2),sensor_locations(:,1)];
im = imread('scenario.png');
im = rgb2gray(im);
im(im<255) = 0;
threshold = 10;

%sensor_locations = [73,537];
sensor_size = [-50,50,-50,50];
cached_sensor_coverage = initialize_coverage_map( sensor_locations, sensor_size, im,threshold);
weights = ones(size(im));
[value, func_state] = sensor_coverage( sensor_locations, [], sensor_locations, weights, cached_sensor_coverage);
[value1, func_state] = sensor_coverage( sensor_locations, func_state, sensor_locations, weights, cached_sensor_coverage);
display_coverage_map( im, sensor_locations,cached_sensor_coverage);