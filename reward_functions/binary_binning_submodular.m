%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [value, func_state] = binary_binning_submodular( node, func_state, bin_size, max_xy, num_nodes_per_cell)
%BINARY_BINNING Summary of this function goes here
%   Detailed explanation goes here
% we will get n^2
if(isempty(func_state) || isempty(func_state.visited_map))
    func_state.visited_map = zeros(ceil(max_xy(1)/bin_size), ceil(max_xy(2)/bin_size));
end

node = [node(:,1), node(:,2)];
node(node(:,1)>max_xy(1),:) = [];
node(node(:,2)>max_xy(2),:) = [];

node(node(:,1)<0,:) = [];
node(node(:,2)<0,:) = [];

n = ceil(node(:,1:2)/bin_size);

IND = sub2ind(size(func_state.visited_map),n(:,1),n(:,2));

v = func_state.visited_map(IND);
func_state.visited_map(IND) = func_state.visited_map(IND) + 1;
v(v>=num_nodes_per_cell) = [];
value = length(v);
end

