%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ value, func_state ] = constant_reward_func( node, func_state, bin_size, max_xy, num_nodes_per_cell)
%CONSTANT_REWARD_FUNC Summary of this function goes here
%   Detailed explanation goes here
value = 1;
func_state = [];

end

