function [ cached_visibility ] = cache_visibility( sensor_locations, map, map_struct, allowed_distances, camera_handle )
%CACHE_VISIBILITY Summary of this function goes here
%   Detailed explanation goes here
for i=1:size(sensor_locations,1)
    cached_visibility{i} =get_observed_ids( sensor_locations(i,:), map, map_struct, allowed_distances, camera_handle );
end
return

