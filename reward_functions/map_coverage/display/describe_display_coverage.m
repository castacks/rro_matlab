%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function describe_display_coverage( init_pos, start_node, end_node )
%DISPLAY_GRAPH Summary of this function goes here
%   Detailed explanation goes here
    plot(init_pos(1), init_pos(2),'.','MarkerSize',60,'MarkerEdgeColor',[0.2 0.7 0.2]) % start/end position
    
    best_route = [init_pos(1), init_pos(2)-5; init_pos(1)+10, init_pos(2)-5];
    plot(best_route(:,1), best_route(:,2),'LineWidth', 4,'Color', [0.4 0.5 0.8]) % best route
    plot(best_route(:,1), best_route(:,2),'.','MarkerSize',60,'MarkerEdgeColor',[0.6 0.6 0.6]) % best route
    plot(best_route(:,1), best_route(:,2),'.','MarkerSize',30,'MarkerEdgeColor',[0.2 0.2 0.2]) % best route
    
    plot(init_pos(1), init_pos(2)-10,'.','MarkerSize',60,'MarkerEdgeColor',[0.8 0.2 0.2]) % sampled node
    
    searched_route = [init_pos(1), init_pos(2)-15; init_pos(1)+5, init_pos(2)-15];
    plot(searched_route(:,1), searched_route(:,2),'LineWidth',4, 'Color', [0.9 0.9 0.9])
    plot(searched_route(:,1), searched_route(:,2),'.','MarkerSize',10,'MarkerEdgeColor',[0.6 0.6 0.6]) % searched routes
        
    plot(init_pos(1), init_pos(2)-20,'s','MarkerSize',15,'MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0,0,0])
    %plot(init_pos(1), init_pos(2)-25,'.','MarkerSize',30,'MarkerEdgeColor',[0.6 0.6 0.6])
    %plot(start_node(1), start_node(2),'.','MarkerSize',60,'MarkerEdgeColor',[0.2 0.7 0.2])
    %plot(start_node(1), start_node(2),'.','MarkerSize',30,'MarkerEdgeColor',[0.1 0.5 0.1])
    %plot(end_node(1), end_node(2),'.','MarkerSize',60,'MarkerEdgeColor',[0.2 0.7 0.2])
    %plot(end_node(1), end_node(2),'.','MarkerSize',30,'MarkerEdgeColor',[0.1 0.5 0.1])

    
    text(init_pos(1)+15,init_pos(2),'Start/End Position','FontSize', 14)
    text(init_pos(1)+15,init_pos(2)-5,'Best Route','FontSize', 14)
    text(init_pos(1)+15,init_pos(2)-10,'Sampled Route','FontSize', 14)
    text(init_pos(1)+15,init_pos(2)-15,'Searched Routes','FontSize', 14)
    text(init_pos(1)+15,init_pos(2)-20,'High Value Region','FontSize', 14)
    
    %text(init_pos(1)+2.5,init_pos(2)-4,'Node Reward: 1.0','FontSize', 14)
    %text(2.0,14,'Find a route that maximizes reward within','FontSize', 24)
    %text(2.0,12,'a pathlength budget of 50','FontSize', 24)
end
