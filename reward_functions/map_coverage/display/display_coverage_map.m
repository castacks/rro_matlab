function display_coverage_map( im, sensor_ids, sensor_locations,cached_sensor_coverage)
%DISPLAY_COVERAGE_MAP Summary of this function goes here
%   Detailed explanation goes here
hold on
axis([1,size(im,1),1,size(im,2)])
map = [0.0,0.0,0.0;1,1,1];
dp_image = 255*imrotate(im,90);
image([1,size(im,1)],[size(im,2),1],dp_image)
colormap(map)

for i=1:length(sensor_ids)
    [px,py] = ind2sub(size(im),cached_sensor_coverage{sensor_ids(i)});
    plot(px(:),py(:),'r.','MarkerSize',10)
end
plot(sensor_locations(sensor_ids(:),1),sensor_locations(sensor_ids(:),2),'bs','MarkerSize',10,'LineWidth',5)
axis equal
end

