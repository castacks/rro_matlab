function [ ground_truth_semantic_map ] = generate_semantic_map( map )
%GENERATE_SEMANTIC_MAP Summary of this function goes here
%   Detailed explanation goes here
c_list = unique(map(:));
c_list = sort(c_list);

[X,Y] = meshgrid(1:size(map,2),1:size(map,1));
IND = sub2ind(size(map),X(:),Y(:));
semantic_map = map;
semantic_map(:) = 1:length(IND(:));
class_probability = zeros(length(IND),length(c_list));

for i=1:length(c_list)
    class_probability(semantic_map(map==c_list(i)),i) = 1;
end
ground_truth_semantic_map.id_map = semantic_map;
ground_truth_semantic_map.p = class_probability;
return;