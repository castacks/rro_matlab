function [ map ] = get_multiclass_map_from_image( image_path,sub_sample)
%GETMAPFROMIMAGE Summary of this function goes here
%   Detailed explanation goes here

im = imread(image_path);
im = rgb2gray(im);
new_size = 1/sub_sample;
im = imresize(im,new_size);
im = double(im);
im = im./max(im(:));
im(im<=0.5) = 0.0;
im(im>0.5) = 1.0;
map = im;
return;