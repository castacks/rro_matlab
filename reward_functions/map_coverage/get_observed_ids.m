function [ IND ] = get_observed_ids( pose, map, map_struct, allowed_distances, camera_handle )
%GET_OBSERVED_IDS Summary of this function goes here
%   Detailed explanation goes here
[X,Y] = camera_handle(pose,map_struct);
coords = [X(:), Y(:)];
[ ID, valid ] = get_valid_ids( coords, map,map_struct);
ID(:,3) = 0;
ID(valid(:)==0,:) = [];
distances = bsxfun(@minus,ID,pose);
distances = sum(distances.*distances,2);
distances = sqrt(distances);

IND = sub2ind(size(map),ID(:,1),ID(:,2));

observations = map(IND);

visited_IND = [];
for i=1:length(allowed_distances)
    temp_distances = distances(distances<=allowed_distances(i));
    temp_IND = IND(distances<=allowed_distances(i));
    temp_IND(map(temp_IND)<(i-1)) = [];
    visited_IND = [visited_IND;temp_IND];
end

IND = unique(visited_IND);