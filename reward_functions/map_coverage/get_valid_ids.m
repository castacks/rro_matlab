function [ ID, valid ] = get_valid_ids( coords, map,map_struct)
    ID = coord2index(coords(:,1),coords(:,2),map_struct.scale,map_struct.min(1),map_struct.min(2));
    valid = ones(size(ID,1),1);
    
    valid(ID(:,1)>size(map,1))=0;
    valid(ID(:,1)<1)=0;
    
    valid(ID(:,2)>size(map,2))=0;
    valid(ID(:,2)<1)=0;   
    
    
end
