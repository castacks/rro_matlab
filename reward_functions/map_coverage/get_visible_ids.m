function [ loc_ids, observed_positions ] = get_visible_ids(pose,camera_handle,semantic_maps,map_struct)
%GET_VISIBLE_IDS Summary of this function goes here
%   Detailed explanation goes here
[X,Y] = camera_handle(pose(1:3),map_struct);
observed_positions = [X(:),Y(:)];
[ ID, valid ] = get_valid_ids( observed_positions, semantic_maps.id_map, map_struct);
ID = ID(valid==1,:);
observed_positions = observed_positions(valid==1,:);
loc_ids = sub2ind(size(semantic_maps.id_map),ID(:,1), ID(:,2));
end

