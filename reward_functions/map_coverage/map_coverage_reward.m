function [value, func_state] = map_coverage_reward( node, func_state, sensor_locations, weights, cached_sensor_coverage)
%MAP_COVERAGE_REWARD Summary of this function goes here
%   Detailed explanation goes here
if (isempty(cached_sensor_coverage) || (size(cached_sensor_coverage,2) ~= size(sensor_locations,1)))
    error('Sensor Coverage function needs cached_sensor_coverage with correct dimnesionality.')
end

if(isempty(func_state) || isempty(func_state.visited_map))
    func_state.visited_map = zeros(size(weights));
end

node(node(:,1)>size(weights,1),:) = [];
node(node(:,2)>size(weights,2),:) = [];

node(node(:,1)<0,:) = [];
node(node(:,2)<0,:) = [];

n = ceil(node(:,1:2));

ind_list = [];
for i=1:size(sensor_locations,1)
   d = bsxfun(@minus,node,sensor_locations(i,:));
   d = sum(d.*d,2);
   
   if min(d)<0.01
       ind_list = [ind_list;cached_sensor_coverage{i}];
   end
end
ind_list = unique(ind_list);
ind_list(func_state.visited_map(ind_list)>0) = [];

new_visits = weights(ind_list);
value = sum(new_visits);

func_state.visited_map(ind_list) = 1;
return;