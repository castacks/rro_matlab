image_path = 'semantic_map.png';
sub_sample = 1;
[ map ] = get_multiclass_map_from_image( image_path, sub_sample);
map_struct.scale = 1; map_struct.min = [1,1];
[ semantic_map ] = generate_semantic_map( map );

camera_fov = deg2rad(120);
camera_handle = @(pose,map_struct_var)(get_circular_camerafootprint( pose, camera_fov, map_struct));