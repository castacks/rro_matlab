%% setting up map
working_dir = pwd;
addpath(strcat(pwd,'/display'));
image_path = 'semantic_map.png';
sub_sample = 1;
[ map ] = get_multiclass_map_from_image( image_path, sub_sample);
map_struct.scale = 1; map_struct.min = [1,1];

%% setting up observation
camera_fov = deg2rad(25);
camera_handle = @(pose,map_struct_var)(get_circular_camerafootprint( pose, camera_fov, map_struct));
allowed_distances = [20;100];

%% creating possible sensor locations
[X,Y] = meshgrid(1:4:size(map,1),1:4:size(map,2));%low sensors
sensor_locations = [X(:),Y(:),10*ones(size(X(:),1),1)];
temp_sensor_locations = sensor_locations;% high sensors
temp_sensor_locations(:,3) = 55;
sensor_locations = [sensor_locations;temp_sensor_locations];
%% caching visibility data
[ cached_visibility ] = cache_visibility( sensor_locations, map, map_struct, allowed_distances, camera_handle );
%% checking reward function
weights = map;
high_reward = 10;
low_reward = 1;
weights(map==0) = high_reward;
weights(map==1) = low_reward;

[value, func_state] = map_coverage_reward( [25,25,10], [], sensor_locations, weights, cached_visibility)
[value, ~] = map_coverage_reward( [29,25,10], func_state, sensor_locations, weights, cached_visibility)
[value, func_state] = map_coverage_reward( [25,25,55], func_state, sensor_locations, weights, cached_visibility)

%% display
sensor_ids = 1;
display_coverage_map( map, sensor_ids, sensor_locations,cached_visibility)