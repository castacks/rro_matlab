camera_fov = deg2rad(120);
camera_handle = @(pose,map_struct_var)(get_circular_camerafootprint( pose, camera_fov, map_struct));
