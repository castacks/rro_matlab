%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
func_state = [];
nodes = 10*rand(10,2);
bin_size = 1;
max_xy = [10, 10];
num_nodes_per_cell = 3;

[ value, func_state ] = binary_binning_submodular( nodes, func_state, bin_size, max_xy, num_nodes_per_cell);
display(value)

[ value, func_state ] = binary_binning_submodular( nodes, func_state, bin_size, max_xy, num_nodes_per_cell);
display(value)

[ value, func_state ] = binary_binning_submodular( nodes, func_state, bin_size, max_xy, num_nodes_per_cell);
display(value)