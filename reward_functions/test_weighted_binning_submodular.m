%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
func_state = [];
nodes = 9*rand(10,2);

bin_size = 1;
max_xy = [10, 10];
num_nodes_per_cell = 2;

weights = ones(10,10);
weights(10,10) = 100;

[ value, func_state ] = weighted_binning_submodular( nodes, func_state, bin_size, weights, num_nodes_per_cell);
display(value)
nodes = [nodes;[10,10]];
[ value, func_state ] = weighted_binning_submodular( nodes, func_state, bin_size, weights, num_nodes_per_cell);
display(value)

[ value, func_state ] = weighted_binning_submodular( nodes, func_state, bin_size, weights, num_nodes_per_cell);
display(value)

[ value, func_state ] = weighted_binning_submodular( nodes, func_state, bin_size, weights, num_nodes_per_cell);
display(value)