%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [value, func_state] = weighted_binning_submodular( node, func_state, bin_size, weights, num_nodes_per_cell)
%BINARY_BINNING Summary of this function goes here
%   Detailed explanation goes here
% we will get n^2
max_xy = bin_size*size(weights);

if(isempty(func_state) || isempty(func_state.visited_map))
    func_state.visited_map = zeros(size(weights));
end
node = [node(:,1), node(:,2)];

node(node(:,1)>max_xy(1),:) = [];
node(node(:,2)>max_xy(2),:) = [];

node(node(:,1)<0,:) = [];
node(node(:,2)<0,:) = [];

n = ceil(node(:,1:2)/bin_size);

IND = sub2ind(size(func_state.visited_map),n(:,1),n(:,2));
temp_map = zeros(size(weights));
for i=1:length(IND)
    temp_map(IND(i)) = temp_map(IND(i)) + 1;
end
new_visits = temp_map(IND) + func_state.visited_map(IND);
new_visits(new_visits>num_nodes_per_cell) = num_nodes_per_cell;

contribution = new_visits - func_state.visited_map(IND);
w = weights(IND);
value = sum(contribution.*w);

func_state.visited_map(IND) = new_visits;
end