%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ route_list, sorted_id, values ] = best_rate_path_selection(route_list, D, nodes, node_id, reward_func_handle, budget)
%BEST_RATE_PATH_SELECTION Summary of this function goes here
%   Detailed explanation goes here
[ route_list, dist_sorted_id, dist_values ] = distance_based_route_selector(route_list, D, nodes, node_id, reward_func_handle, budget);
reward = zeros(length(route_list),1);
for i=1:length(route_list)    
    [reward(i), ~] = reward_func_handle(nodes(node_id,:), route_list(i).reward_func_input);
end

values = reward./dist_values(dist_sorted_id);
[~, sorted_id] = sort(values,1,'descend');
end

