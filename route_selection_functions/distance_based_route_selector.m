%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ route_list, sorted_id, values ] = distance_based_route_selector(route_list, D, nodes, node_id, reward_func_handle, budget)
%DISTANCE_BASED_ROUTE_SELECTOR Summary of this function goes here
%   Detailed explanation goes here
d = zeros(length(route_list),1);
for i=1:length(route_list)
    d(i) = distance_from_route( nodes(node_id,:), route_list(i).route, nodes );
end
[~,sorted_id] = sort(d,1,'ascend');
values = d;
end