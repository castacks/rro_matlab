%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [route_list, best_route_id] = rro( nodes, D, start_node_id, end_node_id, budget, run_time_budget, reward_func_handle, sampling_func_handle, route_selection_func_handle, threshold_distance, threshold_benefit, display_flag, display_func,w)
%RRO Summary of this function goes here
%   Detailed explanation goes here
    if nargin <14
        w=1;
    end
   node_id_list = 1:size(nodes,1);
   sampled_route = set_sampling(nodes);
   sampled_route(start_node_id) = 0;
   sampled_route(end_node_id) = 0;   
   sampled_route_ids = node_id_list(sampled_route==1);
   
   [route_list.route, route_list.cost] = tsp(D, start_node_id, end_node_id,sampled_route_ids);
   route_list.reward_func_input = [];
   
   sampled_route = route_list;
   sampled_route.reward = [];

   if route_list.cost <= budget
       [route_list.reward, route_list.reward_func_input] = reward_func_handle(nodes(route_list.route,:), route_list.reward_func_input);
       sampled_route = route_list;
       best_route_id =  1;
   else
    route_list.reward_func_input = [];
    [route_list.route, route_list.cost] = tsp(D, start_node_id, end_node_id,[]);
    [route_list.reward, route_list.reward_func_input] = reward_func_handle(nodes(route_list.route,:), route_list.reward_func_input);

   end
   
    best_route_id = [];

	if(display_flag)
        active_fig_handle = figure;
	end

        run_time = 0;

    if(route_list.cost >= budget)
        return;
    end

    best_route_id = 1;
    max_route_reward = route_list.reward;
    
    sampling_func_input.reward_func_handle = reward_func_handle;
    sampling_func_input.sampling_func_ip = [];
    sampling_func_input.useless_node_ids = [find(D(start_node_id,:)>budget*0.5)';...
                                            find(D(end_node_id,:)>budget*0.5)'];
    
    

    counter = 1;
    node_id = [];
    best_route_id  = [];
    while (run_time < run_time_budget)
        if(display_flag)
			display_func(counter, nodes, start_node_id, run_time, end_node_id, route_list, best_route_id, node_id, active_fig_handle )
			pause(0.1)
        end
        tic
        [node_id, sampling_func_input] = sampling_func_handle(nodes,sampling_func_input);
        sampled_route = flip_belongingness( node_id, sampled_route, D ,start_node_id, end_node_id, nodes, reward_func_handle,1);
        route_list = add_route2route_list( sampled_route, route_list, budget);
        [route_list, route_id] = select_route_to_add_node(route_list, start_node_id, end_node_id, D, node_id, nodes, budget, reward_func_handle, route_selection_func_handle,w);        
        if(isempty(route_id))
            sampling_func_input.useless_node_ids(end+1) = node_id;
        else
            route_list(route_id) = add_nearby_nodes( nodes, route_list(route_id), reward_func_handle, D, budget, threshold_distance, threshold_benefit);
            
            if(route_list(route_id).reward > max_route_reward)
                %route_list(route_id).run_time = run_time;
                best_route_id = route_id;
                max_route_reward = route_list(route_id).reward;
                sprintf('Reward:%f Cost:%f Size of route:%f', route_list(best_route_id).reward, route_list(best_route_id).cost, length(route_list(best_route_id).route))
                sprintf('Runtime:%f',run_time)
            end
        end

        run_time = run_time + toc;
		counter = counter+1;
    end

end