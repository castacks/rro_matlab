%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ id ] = resampling_multinomial( w, NumSamples)
%RESAMPLING_MULTINOMIAL Summary of this function goes here
%   Detailed explanation goes here
w = w/sum(w);
N = length(w);
Q = cumsum(w);

T = rand(NumSamples,1);
id = zeros(size(T));
for i=1:size(T,1)
    id(i) = find(Q>T(i),1,'first');
end

% T = linspace(0,1-1/N,N) + rand(1)/N;
% T(N+1) = 1;
% 
% i=1;
% j=1;
% 
% while (i<=NumSamples),
%     if (T(i)<Q(j)),
%         id(i)=j;
%         i=i+1;
%     else
%         j=j+1;        
%     end
% end
end

