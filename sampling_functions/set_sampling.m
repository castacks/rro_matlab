%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ selected_nodes ] = set_sampling( nodes )
%SET_SAMPLING Summary of this function goes here
%   Detailed explanation goes here
 selected_nodes = rand(size(nodes,1),1);
 selected_nodes(selected_nodes<=0.5) = 0;
 selected_nodes(selected_nodes>0.5) = 1;
end