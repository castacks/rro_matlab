%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ node_id, sampling_func_input ] = weighted_node_sampling(nodes, sampling_func_input)
%WEIGHTED_NODE_SAMPLING Summary of this function goes here
%   Detailed explanation goes here

 if(isempty(sampling_func_input.sampling_func_ip))
     sampling_func_input.sampling_func_ip.visits = zeros(size(nodes,1),1);
     sampling_func_input.sampling_func_ip.values = zeros(size(nodes,1),1);
     for i=1:size(nodes,1)
         [sampling_func_input.sampling_func_ip.values(i), ~] = sampling_func_input.reward_func_handle(nodes(i,:), []);
     end
end
    
visits = sampling_func_input.sampling_func_ip.visits;
value = sampling_func_input.sampling_func_ip.values;

c = sqrt(2);
tot_visits = max(1.1,sum(visits));
vv = 0.1+visits;
exploration = c*sqrt(log(tot_visits)*bsxfun(@rdivide,1,vv));
exploitation = value;
w = exploration + exploitation;
temp_node_ids = 1:length(w);
w(sampling_func_input.useless_node_ids) = [];
temp_node_ids(sampling_func_input.useless_node_ids) = [];
node_id = resampling_multinomial(w,1);
node_id = temp_node_ids(node_id);
visits(node_id) = visits(node_id) + 1;

sampling_func_input.sampling_func_ip.visits = visits;

end