%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ route_list, route_id ] = select_route_to_add_node( route_list, start_node_id, end_node_id,D, node_id, nodes, budget,  reward_func_handle, route_selection_func_handle,w)
%SELECT_ROUTE_TO_ADD Summary of this function goes here
%   Detailed explanation goes here
if nargin <9
    w=1;
end

[route_list, sorted_id, ~] = route_selection_func_handle(route_list, D, nodes, node_id, reward_func_handle, budget);

route_id = [];

for i=1:length(sorted_id)
    if(is_in_route(node_id,route_list(sorted_id(i)).route)==0)    
        r_length = distance_from_route(nodes(node_id,:),route_list(sorted_id(i)).route, nodes,w); % Lets do the approx check and make it pass or fail based on that
        if(budget >= (route_list(sorted_id(i)).cost + 2*r_length))
            [route_list(sorted_id(i)).route, route_list(sorted_id(i)).cost] = tsp(D, start_node_id, end_node_id,[node_id,route_list(sorted_id(i)).route]);
            route_id = sorted_id(i);
            break;
        end
    end
end

if(isempty(route_id))
    [route, cost] = tsp(D, start_node_id, end_node_id, node_id);    
    if(cost > budget)
        route_id = [];      
        return;        
    end
    route_list(end+1).route = route; 
    route_list(end).cost = cost;
    route_id = length(route_list);
    [route_list(route_id).reward, route_list(route_id).reward_func_input] = reward_func_handle(nodes(route_list(route_id).route,:),[]);
    return;
end

[reward, route_list(route_id).reward_func_input] = reward_func_handle(nodes(route_list(route_id).route,:), route_list(route_id).reward_func_input);
route_list(route_id).reward = reward + route_list(route_id).reward;
end