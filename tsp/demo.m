%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%

nodes = 10*rand(10,2);
start_node_id = 2;
end_node_id = 10;
nodes_to_visit = [7,8,1,5,4,6,3,9];
D = distmat(nodes);

[ p, path_length ] = tsp( D, start_node_id, end_node_id, nodes_to_visit );

plot(nodes([start_node_id; end_node_id],1), nodes([start_node_id; end_node_id],2),'.','MarkerSize',70,'MarkerEdgeColor',[0.2 0.8 0.2])
hold on
plot(nodes(p(:),1), nodes(p(:),2),'LineWidth',4, 'Color', [0.4    0.5    0.8])
plot(nodes(5,1), nodes(5,2),'.','MarkerSize',60,'MarkerEdgeColor',[0.8 0.2 0.2])
plot(nodes(:,1), nodes(:,2),'.','MarkerSize',40,'MarkerEdgeColor',[0.2 0.2 0.2])
plot(nodes(:,1), nodes(:,2),'.','MarkerSize',20,'MarkerEdgeColor',[0.8 0.8 0.8])