%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function D = distmat(X,w)
%DISTMAT Compute euclidian distance matrix from coordinates
if nargin <2
    w = 1;
end
[n,dim] = size(X);
X(:,dim) = w*X(:,dim);

D = zeros(n);
for j = 1:n
    for k = 1:dim
        v = X(:,k) - X(j,k);
        D(:,j) = D(:,j) + v.*v;
    end
end
D = sqrt(D);