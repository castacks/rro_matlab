%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ p, path_length ] = tsp( D, start_node_id, end_node_id, nodes_to_visit )
%TSP Summary of this function goes here
%   Detailed explanation goes here
if size(nodes_to_visit,2)>1
    nodes_to_visit = nodes_to_visit';
end
fake_node_id = [];
if(start_node_id ~= end_node_id)
    % first will insert a fake node
    fake_node_id = size(D,1)+1;
    D(:,fake_node_id) = 10^6;
    D(fake_node_id,:) = 10^6;
    D(fake_node_id,start_node_id ) = 10^(-6);
    D(start_node_id,fake_node_id ) = 10^(-6);

    D(fake_node_id,end_node_id ) = 10^(-6);
    D(end_node_id,fake_node_id ) = 10^(-6);
    nodes_to_visit = [nodes_to_visit;fake_node_id];
end

relevant_ids = unique([start_node_id;nodes_to_visit;end_node_id]);
dd = zeros(length(relevant_ids));
for i = 1:length(relevant_ids)
    for j= 1:length(relevant_ids)
        dd(i,j) = D(relevant_ids(i), relevant_ids(j));
    end
end

[p,~] = tspsearch(dd, 2);
p = relevant_ids(p)';
p(p==fake_node_id) = [];

s_id = find(p == start_node_id);
e_id = find(p == end_node_id);
ordered_list = [];
if(s_id < e_id)
    ordered_list = p(s_id:-1:1);
    ordered_list = [ordered_list, p(end:-1:e_id)];
else
    ordered_list = p(s_id:1:end);
    ordered_list = [ordered_list, p(1:1:e_id)];
end
p = ordered_list;
path_length = 0;
%% Get distance
for i=1:length(p)-1
    path_length = path_length+ D(p(i),p(i+1));
end
end