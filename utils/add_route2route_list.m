function [ route_list ] = add_route2route_list( route, route_list, budget)
%ADD_ROUTE2ROUTE_LIST Summary of this function goes here
%   Detailed explanation goes here
%Is route in budget
if(route.cost>budget)
    return;
end
%Is it already in the list
for i=1:length(route_list)
    if(isempty(setdiff(route.route, route_list(i).route)))
        return;
    end
end
route_list(end+1) = route;
end
