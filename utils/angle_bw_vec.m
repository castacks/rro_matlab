%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ angle ] = angle_bw_vec( v1, v2 )
%ANGLE_BW_VEC Summary of this function goes here
%   Detailed explanation goes here
d_prod = sum(v1.*v2,2);
n1 = sqrt(sum(v1.*v1,2));
n2 = sqrt(sum(v2.*v2,2));
cosd = d_prod./(n1.*n2);
angle = acos(cosd);
end

