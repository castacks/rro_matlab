%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ d ] = compute_path_length(routes,D)
%COMPUTE_PATH_LENGTH Summary of this function goes here
%   Detailed explanation goes here
d = zeros(1,max(size(routes)));
for i=1:max(size(routes))
    route = routes{i};
    for j=1:max(size(route))-1
        d(i) = d(i) + D(route(j),route(j+1));
    end
    d(i) = d(i) + D(route(end),1);
end