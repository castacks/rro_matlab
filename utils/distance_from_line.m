%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ d ] = distance_from_line(p1,p2, p)
%DISTANCE_ Summary of this function goes here
%   Detailed explanation goes here
if(size(p1,2)==2)
    p1 = [p1,0]; p2 = [p2,0]; p = [p,0]; 
end 

a = p2-p1;
b = p-p1;
d = norm(cross(a,b))/norm(p2-p1);

d1 = norm(p-p1);
d2 = norm(p-p2);

if(angle_bw_vec(p-p1,p2-p1)>deg2rad(90) || angle_bw_vec(p-p2,p1-p2)>deg2rad(90))
    d = min(d1,d2);
end
    
end