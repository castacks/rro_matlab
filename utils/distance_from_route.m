%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ d ] = distance_from_route( nodes, route, all_nodes,w )
%DISTANCE_FROM_ROUTE Summary of this function goes here
%   Detailed explanation goes here
if nargin < 4
    w=1;
end
dim = size(nodes,2);
nodes(:,dim) = w*nodes(:,dim);
all_nodes(:,dim) = w*all_nodes(:,dim);


d = 10^6*ones(size(nodes,1),1);
for i=1:size(nodes,1)
    dd = 10^6;
    for j=1:max(size(route))-1
        id = route(j); id1 = route(j+1);
        d_temp = distance_from_line(all_nodes(id,:), all_nodes(id1,:), nodes(i,:));
        dd = min(dd,d_temp);
    end
    d(i) = dd;
end
