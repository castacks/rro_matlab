%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ route_list ] = flip_belongingness( node_id, route_list, D ,start_node_id, end_node_id, nodes, reward_func_handle,mode)
%FLIP_BELONGINGNESS Summary of this function goes here
%   Detailed explanation goes here
if(nargin<8)
    mode=0;
end


if(~isempty(route_list))
    if(isempty(route_list(end).route(route_list(end).route == node_id)))
    route_list(end).route(route_list(end).route == end_node_id) = [];
    route_list(end).route(route_list(end).route == start_node_id) = [];
    [route, cost] = tsp(D, start_node_id, end_node_id,[node_id,route_list(end).route]);
else
    route_list(end).route(route_list(end).route == node_id) = [];
    route_list(end).route(route_list(end).route == end_node_id) = [];
    route_list(end).route(route_list(end).route == start_node_id) = [];
    [route, cost] = tsp(D, start_node_id, end_node_id,route_list(end).route);
end

    [reward, reward_func_input] = reward_func_handle(nodes(route,:), route_list(end).reward_func_input);
    reward = reward + route_list(end).reward;
else
    [reward, reward_func_input] = reward_func_handle(nodes(route,:), []);
end
if(mode==0)
    route_list(end+1).route = route;
else
    route_list(end).route = route;
end
route_list(end).cost = cost;
route_list(end).reward_func_input = reward_func_input;
route_list(end).reward = reward;
end