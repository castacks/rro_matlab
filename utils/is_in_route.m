%% 
% Copyright (c) 2016 Carnegie Mellon University, Sankalp Arora <asankalp@cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%
%%
function [ in ] = is_in_route( node_id, route )
%IS_IN_ROUTE Summary of this function goes here
%   Detailed explanation goes here
route(route~=node_id) = [];
in = length(route);
end

