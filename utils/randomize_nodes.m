function [ new_nodes ] = randomize_nodes( randomize_radius,nodes,num_nodes_per_node )
%RANDOMIZE_NODES Summary of this function goes here
%   Detailed explanation goes here
new_nodes = nodes;
for i=1:num_nodes_per_node
    new_nodes = [new_nodes;(2*randomize_radius*rand(size(nodes)) - randomize_radius) + nodes];
end

